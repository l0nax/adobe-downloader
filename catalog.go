package main

import "time"

type Catalog struct {
	Created time.Time `json:"created"`
	Updated time.Time `json:"updated"`
	Base    string    `json:"base"`
	ID      string    `json:"id"`
	Type    string    `json:"type"`
	Subtype string    `json:"subtype"`
	Payload struct {
		UserCreated time.Time `json:"userCreated"`
		UserUpdated time.Time `json:"userUpdated"`
		Name        string    `json:"name"`
		Presets     struct {
			Favorites struct{} `json:"favorites"`
		} `json:"presets"`
		Profiles struct {
			Favorites struct{} `json:"favorites"`
		} `json:"profiles"`
		Settings struct {
			Universal struct {
				Connections struct {
					ConnectionAPIKey struct {
						Created time.Time `json:"created"`
					} `json:"connection_api_key"`
				} `json:"connections"`
			} `json:"universal"`
			Desktop struct {
				DesktopKey string `json:"desktop_key"`
			} `json:"desktop"`
			Web struct {
				WebKey string `json:"web_key"`
			} `json:"web"`
			Mobile struct {
				MobileKey string `json:"mobile_key"`
			} `json:"mobile"`
			Photosdk struct {
				SdkKey string `json:"sdk_key"`
			} `json:"photosdk"`
		} `json:"settings"`
	} `json:"payload"`
	Links map[string]CatalogLinkEntry `json:"links"`
}

type CatalogLinkEntry struct {
	Href      string `json:"href"`
	Templated bool   `json:"templated"`
}
