package main

import "time"

type AssetResponse struct {
	Base      string          `json:"base"`
	Resources []AssetResource `json:"resources"`
	Links     struct {
		Prev struct {
			Href string `json:"href"`
		} `json:"prev"`
		RelsAsset struct {
			Href      string `json:"href"`
			Templated bool   `json:"templated"`
		} `json:"/rels/asset"`
	} `json:"links"`
}

type AssetResource struct {
	ID          string    `json:"id"`
	Type        string    `json:"type"`
	Updated     time.Time `json:"updated"`
	Subtype     string    `json:"subtype"`
	Created     time.Time `json:"created"`
	RevisionIds []string  `json:"revision_ids"`
	Links       struct {
		Self struct {
			Href string `json:"href"`
		} `json:"self"`
		RelsProxyCreate struct {
			Href      string `json:"href"`
			Templated bool   `json:"templated"`
		} `json:"/rels/proxy_create"`
		RelsRenditionCreate struct {
			Href      string `json:"href"`
			Templated bool   `json:"templated"`
		} `json:"/rels/rendition_create"`
		RelsVersionCreate struct {
			Href      string `json:"href"`
			Templated bool   `json:"templated"`
		} `json:"/rels/version_create"`
		RelsVersions struct {
			Href string `json:"href"`
		} `json:"/rels/versions"`
		RelsRenditionType2048 struct {
			Href string `json:"href"`
		} `json:"/rels/rendition_type/2048"`
		RelsRenditionType1280 struct {
			Href string `json:"href"`
		} `json:"/rels/rendition_type/1280"`
		RelsRenditionType640 struct {
			Href string `json:"href"`
		} `json:"/rels/rendition_type/640"`
		RelsRenditionTypeThumbnail2X struct {
			Href string `json:"href"`
		} `json:"/rels/rendition_type/thumbnail2x"`
		RelsRenditionGenerateFullsize struct {
			Href      string `json:"href"`
			Templated bool   `json:"templated"`
		} `json:"/rels/rendition_generate/fullsize"`
		RelsProxyType2560 struct {
			Href string `json:"href"`
		} `json:"/rels/proxy_type/2560"`
		RelsMasterCreate struct {
			Href      string `json:"href"`
			Templated bool   `json:"templated"`
		} `json:"/rels/master_create"`
		RelsMainCreateParts struct {
			Href      string `json:"href"`
			Templated bool   `json:"templated"`
		} `json:"/rels/main_create/parts"`
		RelsXmpCreate struct {
			Href      string `json:"href"`
			Templated bool   `json:"templated"`
		} `json:"/rels/xmp_create"`
		RelsXmpDevelopCreate struct {
			Href      string `json:"href"`
			Templated bool   `json:"templated"`
		} `json:"/rels/xmp_develop_create"`
		RelsXmpDevelopCreateParts struct {
			Href      string `json:"href"`
			Templated bool   `json:"templated"`
		} `json:"/rels/xmp_develop_create/parts"`
		RelsVersionsXmpDevelopCreateParts struct {
			Href      string `json:"href"`
			Templated bool   `json:"templated"`
		} `json:"/rels/versions/xmp_develop_create/parts"`
		RelsMaster struct {
			Href        string `json:"href"`
			ContentType string `json:"content_type"`
		} `json:"/rels/master"`
		RelsXmp struct {
			Href string `json:"href"`
		} `json:"/rels/xmp"`
		RelsAuxCreateParts struct {
			Href      string `json:"href"`
			Templated bool   `json:"templated"`
		} `json:"/rels/aux_create/parts"`
		RelsAuxCopyTo struct {
			Href      string `json:"href"`
			Templated bool   `json:"templated"`
		} `json:"/rels/aux_copy_to"`
	} `json:"links"`
	Payload AssetResourcePayload `json:"payload"`
}

type AssetResourcePayload struct {
	Develop struct {
		CroppedWidth    int       `json:"croppedWidth"`
		CroppedHeight   int       `json:"croppedHeight"`
		FromDefaults    bool      `json:"fromDefaults"`
		ProcessingModel string    `json:"processingModel"`
		UserOrientation int       `json:"userOrientation"`
		XmpCameraRaw    string    `json:"xmpCameraRaw"`
		CrsVersion      string    `json:"crsVersion"`
		UserUpdated     time.Time `json:"userUpdated"`
	} `json:"develop"`
	UserUpdated  time.Time `json:"userUpdated"`
	CaptureDate  time.Time `json:"captureDate"`
	ImportSource struct {
		OriginalHeight   int       `json:"originalHeight"`
		ImportedOnDevice string    `json:"importedOnDevice"`
		ImportTimestamp  time.Time `json:"importTimestamp"`
		ContentType      string    `json:"contentType"`
		OriginalCrop     struct {
			Top         int `json:"top"`
			Bottom      int `json:"bottom"`
			Orientation int `json:"orientation"`
			Right       int `json:"right"`
			Left        int `json:"left"`
		} `json:"originalCrop"`
		FileSize       int    `json:"fileSize"`
		FileName       string `json:"fileName"`
		OriginalWidth  int    `json:"originalWidth"`
		ImportedBy     string `json:"importedBy"`
		LocalAssetID   string `json:"localAssetId"`
		Sha256         string `json:"sha256"`
		UniqueDeviceID string `json:"uniqueDeviceId"`
	} `json:"importSource"`
	Xmp struct {
		Tiff struct {
			Orientation string `json:"Orientation"`
			Make        string `json:"Make"`
			Model       string `json:"Model"`
		} `json:"tiff"`
		Exif struct {
			ApertureValue         []int     `json:"ApertureValue"`
			FNumber               []int     `json:"FNumber"`
			MaxApertureValue      []int     `json:"MaxApertureValue"`
			FocalLength           []int     `json:"FocalLength"`
			LightSource           string    `json:"LightSource"`
			DateTimeOriginal      time.Time `json:"DateTimeOriginal"`
			FlashRedEyeMode       bool      `json:"FlashRedEyeMode"`
			ExposureTime          []int     `json:"ExposureTime"`
			FlashFired            bool      `json:"FlashFired"`
			MeteringMode          string    `json:"MeteringMode"`
			FlashFunction         bool      `json:"FlashFunction"`
			FlashMode             string    `json:"FlashMode"`
			FlashReturn           string    `json:"FlashReturn"`
			ISOSpeedRatings       int       `json:"ISOSpeedRatings"`
			ShutterSpeedValue     []int     `json:"ShutterSpeedValue"`
			FocalLengthIn35MmFilm int       `json:"FocalLengthIn35mmFilm"`
			BrightnessValue       []int     `json:"BrightnessValue"`
			ExposureProgram       string    `json:"ExposureProgram"`
			ExposureBiasValue     []int     `json:"ExposureBiasValue"`
		} `json:"exif"`
		Aux struct {
			Lens string `json:"Lens"`
		} `json:"aux"`
		Xmp struct {
			CreatorTool string    `json:"CreatorTool"`
			CreateDate  time.Time `json:"CreateDate"`
			ModifyDate  time.Time `json:"ModifyDate"`
		} `json:"xmp"`
		Photoshop struct {
			DateCreated time.Time `json:"DateCreated"`
		} `json:"photoshop"`
		MwgRs struct {
			Regions struct {
				AppliedToDimensions struct {
					H    int    `json:"h"`
					Unit string `json:"unit"`
					W    int    `json:"w"`
				} `json:"AppliedToDimensions"`
				RegionList struct {
					// XXX: This section has been removed!!
				} `json:"RegionList"`
			} `json:"Regions"`
		} `json:"mwg-rs"`
	} `json:"xmp"`
	UserCreated time.Time `json:"userCreated"`
	Aesthetics  struct {
		Application string    `json:"application"`
		Balancing   int       `json:"balancing"`
		Content     int       `json:"content"`
		Created     time.Time `json:"created"`
		Dof         int       `json:"dof"`
		Emphasis    int       `json:"emphasis"`
		Harmony     int       `json:"harmony"`
		Lighting    int       `json:"lighting"`
		Repetition  int       `json:"repetition"`
		Rot         int       `json:"rot"`
		Score       int       `json:"score"`
		Symmetry    int       `json:"symmetry"`
		Version     int       `json:"version"`
		Vivid       int       `json:"vivid"`
	} `json:"aesthetics"`
	AutoTags struct {
		Tags struct {
			Man        int `json:"man"`
			Beach      int `json:"beach"`
			Male       int `json:"male"`
			Summer     int `json:"summer"`
			Boy        int `json:"boy"`
			Sea        int `json:"sea"`
			Men        int `json:"men"`
			Young      int `json:"young"`
			Couple     int `json:"couple"`
			Woman      int `json:"woman"`
			Fitness    int `json:"fitness"`
			Outdoors   int `json:"outdoors"`
			Attractive int `json:"attractive"`
			Handsome   int `json:"handsome"`
			Guy        int `json:"guy"`
			Sand       int `json:"sand"`
			Active     int `json:"active"`
			Sitting    int `json:"sitting"`
			People     int `json:"people"`
			Stretching int `json:"stretching"`
			Adult      int `json:"adult"`
			Ocean      int `json:"ocean"`
			Happy      int `json:"happy"`
			Water      int `json:"water"`
			Exercise   int `json:"exercise"`
		} `json:"tags"`
		Application string    `json:"application"`
		Version     int       `json:"version"`
		Created     time.Time `json:"created"`
	} `json:"autoTags"`
}

type AdobeFiPeople struct {
	Confirmed bool `json:"confirmed"`
}
