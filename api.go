package main

import (
	"strings"

	"github.com/go-resty/resty/v2"
)

var c *resty.Client

func GetAssets() {
	subtype := []string{
		"image",
		"video",
	}

	exclude := "incomplete"
	embed := "user"

	c.R().
		SetQueryParams(map[string]string{ // sample of those who use this manner
			"subtype":         strings.Join(subtype, ";"),
			"captured_before": "api-secert", // FORMAT: 2023-08-02T14:04:16.000+02:00
			"exclude":         exclude,
			"embed":           embed,
		}).
		Get("/catalogs/{category_id}/assets")
}

func GetAlbums() {
	subtype := []string{
		"image",
		"video",
	} // OR: "collection_set"

	exclude := "incomplete"
	embed := "user"

	c.R().
		SetQueryParams(map[string]string{ // sample of those who use this manner
			"subtype":         strings.Join(subtype, ";"),
			"captured_before": "api-secert", // FORMAT: 2023-08-02T14:04:16.000+02:00
			"exclude":         exclude,
			"embed":           embed,
			"limit":           "500",
		}).
		Get("/catalogs/{category_id}/albums")
}
